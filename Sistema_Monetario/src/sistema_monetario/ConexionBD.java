/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_monetario;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author jose
 */
public class ConexionBD {
    
    public static Connection conexion;
    public static Statement estado;
    public static ResultSet resultado;
    public static CallableStatement call;
    
    public void conectar(){
        
        try {
            String url = "jdbc:oracle:thin:@localhost:1521:Jose20131372";
            conexion = DriverManager.getConnection(url,"hr","1234");
            estado = conexion.createStatement();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos, \"jdbc\"","Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String consultarLogin(String usuario, String pass, String rol){
        try {           
            String consulta = "SELECT rol FROM usuario WHERE"
                            + " usuario = '"+usuario
                            + "' AND password = '"+pass
                            + "' AND rol = '"+rol+"'";
            
            resultado = estado.executeQuery(consulta);            
            while(resultado.next()){
                return resultado.getString("rol");
            }                                    
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(null,"No se ha podido establecer una conexion con la base de datos.");
        }        
        return "Error";
    }
    
    public Long retornarDPITablaDetalle(int idcuenta){
        try {
            String consulta = "SELECT idcliente FROM detalle_cliente WHERE idcuenta = "+idcuenta;            
            resultado = estado.executeQuery(consulta);
            while(resultado.next()){
                return resultado.getLong("idcliente");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al retornar el dpi del cliente");
        }
        return null;
    }
    public boolean consultarDPI(Long id){
        try {
            String consulta = "SELECT dpi FROM cliente WHERE "
                              +"dpi = "+id;
            resultado = estado.executeQuery(consulta);
            while(resultado.next()){                
                return true;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"No se pudo establecer una conexion con la base de datos.","Error-ConsultarDPI",JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean existeEnOtraTabla(int dpi){
        try {
            //Por el momento en las tablas que pueden existir el registro de un cliente 
            //es en la de cliente, y en la tabla de detalle_cliente.
            String consulta = "SELECT * FROM detalle_cliente WHERE idcliente = "+dpi;
            resultado = estado.executeQuery(consulta);
            while(resultado.next()){
                resultado.close();
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
    
    public int retornarID(String nombre, String nombreTabla){
        try {                     
            String consulta = "SELECT id"+nombreTabla +" FROM "+nombreTabla+" WHERE"
                            + " nombre = '"+nombre+"'";
            //System.out.println(consulta);
            resultado = estado.executeQuery(consulta);
            
            if(resultado.next()){                
                return resultado.getInt("id"+nombreTabla);
            }                        
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(null,"No se ha podido establecer una conexion con la base de datos.");
        }        
        return -1;
    }
    
    public boolean consultarID(String id, String nombreTabla){
        try {
            int ide = Integer.valueOf(id);
            String consulta = "SELECT id"+nombreTabla+" FROM " + nombreTabla + " WHERE id"+nombreTabla + " = " + ide;
            resultado = estado.executeQuery(consulta);
            while(resultado.next()){
                return true;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"No se ha podido establecer una conexion con la base de datos.");
        }
        return false;
    }
    
    public String retornarNombreCampo(int id, String nombretabla){
        try {
            String consulta = "SELECT nombre FROM "+nombretabla+" WHERE id"+nombretabla+" = "+id;
            resultado = estado.executeQuery(consulta);
            while(resultado.next()){
                return resultado.getString("nombre");
            }
        } catch (Exception e) {
        }
        return "";
    }
}
