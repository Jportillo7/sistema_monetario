package sistema_monetario;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jose
 */
public class Validaciones {

    public boolean tipoUsuario(String cadena){
        String expresion = "[A-Za-z][A-Za-z]+";
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();        
    }
    
    public boolean tipoNombre(String cadena){
        String expresion = "[A-Za-z\\ ]+";
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();        
    }
    public boolean tipoVarchar(String cadena){
        String expresion = "[A-Za-z0-9]+";
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();        
    }
    public boolean tipoNumerico(String cadena){
        String expresion = "[0-9]+";
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();
    }
    
    public boolean tipoDouble(String cadena){
        String expresion = "[0-9]+[.][0-9][0-9]";
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();
    }
    
    public boolean tipoCorreo(String cadena){        
        String expresion = "[A-Za-z0-9\\.\\_\\-]+[@][A-Za-z]+[.][A-Za-z]+";                
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();
    }
    
    public boolean tipoDireccion(String cadena){
        String expresion = "[A-Za-z0-9\\.\\-\\,\\ ]+";                
        Pattern p = Pattern.compile(expresion);
        Matcher m = p.matcher(cadena);        
        return m.matches();
    }
    
    public int tipoFecha(int dia, int mes, int anio){
        /*1. Dia fuera de rango
          2. Mes fuera de rango
          3. Anio fuera de rango actual          
          4. Dia del mes de febrero fuera del rango
          5. Dia del mes de febrero incorrecta porque no es año bisiesto          
          6. Fecha correcta
        */
        
        boolean bisiesto = false;
        boolean aniomayor = false;
        int digitosanio = Integer.toString(anio).length();        
        
        if(digitosanio == 2){            
            if(anio > 19)
                aniomayor = true;
            if(anio %4==0){
                bisiesto = true;
            }
        }else if(digitosanio == 4){
            int cuarto = anio%10;
            anio = anio/10;
            int tercero = anio%10;
            anio = anio/10;            
            String nuevoanio = String.valueOf(tercero)+String.valueOf(cuarto);            
            int auxanio = Integer.parseInt(nuevoanio);
            if(auxanio %4==0){
                bisiesto=true;
            }
        }        
        
        
        if(!(dia>31)){
            if(!(mes>12)){
                if(!(anio > 2019 || aniomayor)){
                    if (!((mes == 02 || mes == 2) && dia > 29)) {
                        if (dia == 29 && bisiesto) {
                            return 6;
                        } else {
                            //dia del mes de febrero incorrecta ya que no es anio bisiesto
                            return 5;
                        }
                    } else {
                        //dia del mes de febrero fuera de rango
                        return 4;
                    }
                }else{
                    //anio fuera de rango actual
                    return 3;
                }
            }else{
                //mes fuera de rango
                return 2;
            }
        }else{
            //dia fuera de rango
            return 1;
        }                        
    }
    
    public LinkedList camposVacios(LinkedList campos){
        LinkedList<String> listaerrores = new LinkedList<>();
        Campos listaCampos;
        for(Object n : campos){
            listaCampos = (Campos)n;
            if(listaCampos.valorCampo.equals("")){
                String error = "El campo "+listaCampos.nombreCampo+" se encuentra vacio";
                listaerrores.addLast(error);
            }            
        }        
        return listaerrores;
    }
    
    public LinkedList camposCorrectos(LinkedList campos){
        LinkedList<String> listaerrores = new LinkedList<>();
        Campos listaCampos;
        for(Object n : campos){
            listaCampos = (Campos)n;            
            String tipocampo = listaCampos.nombreCampo;
            String error;
            switch(tipocampo){
                case "id":
                    if(!(tipoNumerico(listaCampos.valorCampo.toString()))){
                        error = "El campo \"ID\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "usuario":
                    if(!(tipoUsuario(listaCampos.valorCampo.toString()))){
                        error = "El campo \"USUARIO\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "password":
                    if(!(tipoVarchar(listaCampos.valorCampo.toString()))){
                        error = "El campo \"PASSWORD\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "dpi":
                    if(!(tipoNumerico(listaCampos.valorCampo.toString()))){
                        error = "El campo \"DPI\" contiene caracteres invalidos\n"+"Se requiere que el campo DPI sea de tipo numerico";
                        listaerrores.addLast(error);
                    } 
                break;
                case "nombre":
                    if(!(tipoNombre(listaCampos.valorCampo.toString()))){
                        error = "El campo \"NOMBRE\" contiene caracteres invalidos\n"+"Se requiere que el campo NOMBRE sea de tipo varchar";
                        listaerrores.addLast(error);
                    }
                break;
                case "correo":
                    if(!(tipoCorreo(listaCampos.valorCampo.toString()))){
                        error = "El campo \"CORREO\" contiene caracteres invalidos\n"+"Debe ingresar los datos como se indica en el ejemplo.";
                        listaerrores.addLast(error);
                    }
                break;
                case "telefono":
                    if(!(tipoNumerico(listaCampos.valorCampo.toString()))){
                        error = "El campo \"TELEFONO\" contiene caracteres invalidos\n"+"Se requiere que el campo TELEFONO sea de tipo numerico";
                        listaerrores.addLast(error);
                    }
                break;
                case "direccion":
                    if(!(tipoDireccion(listaCampos.valorCampo.toString()))){
                        error = "El campo \"DIRECCION\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "monto":
                    if(!tipoDouble(listaCampos.valorCampo.toString())){
                        error = "El campo \"monto\" debe ser de tipo double y contener unicamente dos decimales";
                        listaerrores.addLast(error);
                    }
                break;
                case "propietario":
                    if(!tipoNombre(listaCampos.valorCampo.toString())){
                        error = "El campo \"propietario\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "cuenta":
                    if(!tipoNumerico(listaCampos.valorCampo.toString())){
                        error = "El campo \"cuenta\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "cheque":
                    if(!tipoNumerico(listaCampos.valorCampo.toString())){
                        error = "El campo \"cheque\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }
                break;
                case "chequera":
                    if(!tipoNumerico(listaCampos.valorCampo.toString())){
                        error = "El campo \"chequera\" contiene caracteres invalidos";
                        listaerrores.addLast(error);
                    }                    
                break;
            }            
        }        
        return listaerrores;
    }
}
