
package sistema_monetario;

/**
 *
 * @author jose
 */
public class Campos {

    public String nombreCampo;
    public Object valorCampo;
    
    public Campos(String nombre, Object valor){
        this.nombreCampo = nombre;
        this.valorCampo = valor;
    }
}
