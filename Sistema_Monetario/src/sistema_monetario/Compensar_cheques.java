/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_monetario;

/**
 *
 * @author jose
 */
public class Compensar_cheques {
    public String banco;
    public String referencia;
    public String cuenta;
    public String nocheque;
    public String monto;
    public String estado;
    
    public Compensar_cheques(String banco, String ref, String cuenta, String nocheque, String monto,String estado){
        this.banco = banco;
        this.referencia = ref;
        this.cuenta = cuenta;
        this.nocheque = nocheque;
        this.monto = monto;
        this.estado = estado;
    }
}
