/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.awt.HeadlessException;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import sistema_monetario.Campos;
import sistema_monetario.ConexionBD;
import static sistema_monetario.ConexionBD.conexion;

/**
 *
 * @author jose
 */
public class crearCuenta extends javax.swing.JFrame {

    ConexionBD bd = new ConexionBD();
    
    public crearCuenta() {
        initComponents();
        this.cmbagencia.removeAllItems();
        try {
            bd.conectar();
            ConexionBD.resultado = ConexionBD.estado.executeQuery("SELECT * FROM agencia");
            while(ConexionBD.resultado.next()){
                this.cmbagencia.addItem(ConexionBD.resultado.getString("nombre"));
            }
            ConexionBD.resultado.close();
            ConexionBD.conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Erro al realizar la consulta para llenar el campo de agencias","Llenar agencias",JOptionPane.ERROR_MESSAGE);
        }
        
        try {
            bd.conectar();
            ConexionBD.resultado = ConexionBD.estado.executeQuery("SELECT count(idcuenta) as cantidad FROM cuenta");
            while(ConexionBD.resultado.next()){
                int cantidad = ConexionBD.resultado.getInt("cantidad")+1;
                this.txtidcuenta.setText("00"+cantidad);
            }
            ConexionBD.resultado.close();
            ConexionBD.conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Erro al realizar la consulta para el id de la cuenta","Error",JOptionPane.ERROR_MESSAGE);
        }
        this.txtidcuenta.setEnabled(false);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtdpi = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtidcuenta = new javax.swing.JTextField();
        btnCREARCUENTA = new javax.swing.JButton();
        btnBACKCUENTA = new javax.swing.JButton();
        cmbtipo = new javax.swing.JComboBox();
        cmbagencia = new javax.swing.JComboBox();

        jLabel10.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("DPI:");

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Nombre:");

        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Apellido:");

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Telefono:");

        jTextField11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField11ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Direccion:");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Aperturar cuenta");

        jPanel1.setBackground(new java.awt.Color(153, 204, 255));

        jLabel11.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("DPI:");

        txtdpi.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtdpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdpiActionPerformed(evt);
            }
        });
        txtdpi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdpiKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Tipo cuenta:");

        jLabel6.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Nombre Agencia");

        jLabel8.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Id cuenta:");

        txtidcuenta.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtidcuenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtidcuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidcuentaActionPerformed(evt);
            }
        });

        btnCREARCUENTA.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnCREARCUENTA.setText("CREAR");
        btnCREARCUENTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCREARCUENTAActionPerformed(evt);
            }
        });

        btnBACKCUENTA.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnBACKCUENTA.setText("BACK");
        btnBACKCUENTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBACKCUENTAActionPerformed(evt);
            }
        });

        cmbtipo.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        cmbtipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "MONETARIA", "AHORRO" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCREARCUENTA, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnBACKCUENTA, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtidcuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbtipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbagencia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(102, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtidcuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cmbtipo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbagencia, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(62, 62, 62)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBACKCUENTA, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(btnCREARCUENTA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void txtdpiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdpiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdpiActionPerformed

    private void txtidcuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidcuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidcuentaActionPerformed

    private void btnBACKCUENTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBACKCUENTAActionPerformed
        this.txtdpi.setText("");
        this.txtidcuenta.setText("");
        
        try {
            bd.conectar();
            ConexionBD.resultado = ConexionBD.estado.executeQuery("SELECT count(idcuenta) as cantidad FROM cuenta");
            while(ConexionBD.resultado.next()){
                int cantidad = ConexionBD.resultado.getInt("cantidad")+1;
                this.txtidcuenta.setText("00"+cantidad);
            }
            ConexionBD.resultado.close();
            ConexionBD.conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Erro al realizar la consulta para el id de la cuenta","Error",JOptionPane.ERROR_MESSAGE);
        }
        
        this.dispose();        
        Principal.empleado.setVisible(true);
    }//GEN-LAST:event_btnBACKCUENTAActionPerformed

    private void btnCREARCUENTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCREARCUENTAActionPerformed
        try {
            String idcuenta = txtidcuenta.getText();
            String dpi = txtdpi.getText();
            String tipo = cmbtipo.getSelectedItem().toString().toLowerCase();
            String nombreAgencia = cmbagencia.getSelectedItem().toString();
            
            Principal.listacampos.addLast(new Campos("id",idcuenta));
            Principal.listacampos.addLast(new Campos("dpi",dpi));                        

            LinkedList<String> validar1, validar2;
            validar1 = Principal.validar.camposVacios(Principal.listacampos);
            validar2 = Principal.validar.camposCorrectos(Principal.listacampos);
            
            if(validar1.size()>0){
                for(String mensaje : validar1){
                    JOptionPane.showMessageDialog(null,mensaje,"Adevertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                validar1.clear();
            }else if(validar2.size()>0){
                for(String mensaje : validar2){
                    JOptionPane.showMessageDialog(null,mensaje,"Adevertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                validar2.clear();
            }else{
                bd.conectar();
                int idagencia2 = bd.retornarID(nombreAgencia,"agencia");
                ConexionBD.conexion.close();
                bd.conectar();
                boolean idcuenta2 = bd.consultarID(idcuenta, "cuenta");
                ConexionBD.conexion.close();
                bd.conectar();
                boolean dpi2 = bd.consultarDPI(Long.parseLong(dpi));
                ConexionBD.conexion.close();
                bd.conectar();
                String estado_cliente = "";
                String consulta_estado_cliente = "SELECT estado FROM cliente WHERE dpi = "+Long.parseLong(dpi);
                ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_estado_cliente);
                while(ConexionBD.resultado.next()){
                    estado_cliente = ConexionBD.resultado.getString("estado");
                }
                ConexionBD.conexion.close();
                if (idagencia2 != -1) {
                    if(!idcuenta2){
                        if (dpi2) {
                            if (estado_cliente.equals("activo")) {
                                //procedimiento almacenado de cuenta
                                bd.conectar();
                                ConexionBD.call = conexion.prepareCall("{call insertar_tabla_cuenta(?,?,?,?,?)}");
                                ConexionBD.call.setInt(1, Integer.valueOf(idcuenta));
                                ConexionBD.call.setInt(2, idagencia2);
                                ConexionBD.call.setString(3, tipo);
                                ConexionBD.call.setString(4, "activada");
                                ConexionBD.call.setInt(5, 500);
                                ConexionBD.call.execute();

                                //procedimiento almacenado de detalle_cliente
                                bd.conectar();
                                ConexionBD.call = conexion.prepareCall("{call insertar_tabla_detalle_cliente(?,?)}");
                                ConexionBD.call.setLong(1, Long.parseLong(dpi));
                                ConexionBD.call.setInt(2, Integer.valueOf(idcuenta));
                                ConexionBD.call.execute();
//                            String consulta = "INSERT INTO cuenta VALUES(" + idcuenta + ","
//                                    + idagencia2 + "," + "'" + tipo + "'"+", 'activada', 500" + ")\n";
//                            String consulta2 = "INSERT INTO detalle_cliente VALUES(" + dpi + "," + idcuenta + ")";
//                            ConexionBD.estado.execute(consulta);
//                            ConexionBD.estado.execute(consulta2);
                                JOptionPane.showMessageDialog(null, "Cuenta registrada exitosamente","Exito",JOptionPane.INFORMATION_MESSAGE);
                                this.txtdpi.setText("");
                                ConexionBD.conexion.close();
                                try {
                                    bd.conectar();
                                    ConexionBD.resultado = ConexionBD.estado.executeQuery("SELECT count(idcuenta) as cantidad FROM cuenta");
                                    while (ConexionBD.resultado.next()) {
                                        int cantidad = ConexionBD.resultado.getInt("cantidad") + 1;
                                        this.txtidcuenta.setText("00" + cantidad);
                                    }
                                    ConexionBD.resultado.close();
                                    ConexionBD.conexion.close();
                                } catch (Exception e) {
                                    JOptionPane.showMessageDialog(null, "Erro al realizar la consulta para el id de la cuenta", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            }else{
                                bd.conectar();
                                String nombrecliente = "";
                                String consulta_nombre_cliente= "SELECT nombre FROM cliente WHERE dpi = "+Long.parseLong(dpi);
                                ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_nombre_cliente);
                                while(ConexionBD.resultado.next()){
                                    nombrecliente = ConexionBD.resultado.getString("nombre");                                            
                                }
                                ConexionBD.estado.close();
                                JOptionPane.showMessageDialog(null,"No se puede aperturar una cuenta al cliente "+nombrecliente+" debido a que contiene estado de \"Eliminado\""
                                +"\nSe debe cambiar de cliente o actualizar el estado del mismo.","Adevertencia",JOptionPane.WARNING_MESSAGE);
                            }
                        }else{
                            JOptionPane.showMessageDialog(null, "El dpi del cliente no existe en la base de datos.","Adevertencia",JOptionPane.WARNING_MESSAGE);
                                    
                            ConexionBD.conexion.close();
                        }
                    }else{
                        JOptionPane.showMessageDialog(null, "El id de la cuenta ya fue ingresado a la base de datos."+"\nNo pueden existir datos duplicados","Adevertencia",JOptionPane.WARNING_MESSAGE);
                        ConexionBD.conexion.close();
                    }                    
                }else{
                    JOptionPane.showMessageDialog(null, "Error al retornar el id de la agencia.","Error",JOptionPane.ERROR_MESSAGE);
                    ConexionBD.conexion.close();
                }                                
            }            
        } catch (HeadlessException | SQLException | NumberFormatException e) {
            System.out.println("Error Capturado: "+e);
            JOptionPane.showMessageDialog(null, "No es posible registrar la cuenta","Error",JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btnCREARCUENTAActionPerformed

    private void txtdpiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdpiKeyTyped
        int caracteres = 13;
        if (this.txtdpi.getText().length() >= caracteres) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "EL numero de caracteres en el campo dpi"
                    + "\ndebe ser igual a 13", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtdpiKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(crearCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(crearCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(crearCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(crearCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new crearCuenta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBACKCUENTA;
    private javax.swing.JButton btnCREARCUENTA;
    private javax.swing.JComboBox cmbagencia;
    private javax.swing.JComboBox cmbtipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField txtdpi;
    private javax.swing.JTextField txtidcuenta;
    // End of variables declaration//GEN-END:variables
}
