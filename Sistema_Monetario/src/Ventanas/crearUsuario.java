/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.util.LinkedList;
import javax.swing.JOptionPane;
import sistema_monetario.Campos;
import sistema_monetario.ConexionBD;

/**
 *
 * @author jose
 */
public class crearUsuario extends javax.swing.JFrame {

    ConexionBD bd = new ConexionBD();
    
    public crearUsuario() {
        initComponents();
        this.cmbagencia.removeAllItems();
        try {
            bd.conectar();
            ConexionBD.resultado = ConexionBD.estado.executeQuery("SELECT * FROM agencia");
            while(ConexionBD.resultado.next()){
                this.cmbagencia.addItem(ConexionBD.resultado.getString("nombre"));
            }
            ConexionBD.resultado.close();
            ConexionBD.conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Erro al realizar la consulta para llenar el campo de agencias");
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtidusuario = new javax.swing.JTextField();
        txtusuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmbrol = new javax.swing.JComboBox();
        cmbagencia = new javax.swing.JComboBox();
        btnCREARUSUARIO = new javax.swing.JButton();
        btnBACKUSUARIO = new javax.swing.JButton();
        txtpass = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        txtnombreapellido = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Crear Usuario");

        jPanel1.setForeground(new java.awt.Color(153, 204, 255));

        jLabel1.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel1.setText("Id Usuario:");

        txtidusuario.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N

        txtusuario.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        txtusuario.setEnabled(false);

        jLabel2.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel2.setText("Nombre Usuario:");

        jLabel3.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel3.setText("Password usuario:");

        jLabel4.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel4.setText("Rol Usuario:");

        jLabel5.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel5.setText("Agencia Usuario:");

        cmbrol.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        cmbrol.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "CAJERO", "GERENTE", "AUDITOR", "RECEPTOR/PAGADOR", "ATENCION_AL_CLIENTE" }));

        cmbagencia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "INDUSTRIAL", "GYT", "BANTRAB", "BARURAL", "AGROMERCANTIL" }));

        btnCREARUSUARIO.setText("CREAR");
        btnCREARUSUARIO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCREARUSUARIOActionPerformed(evt);
            }
        });

        btnBACKUSUARIO.setText("BACK");
        btnBACKUSUARIO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBACKUSUARIOActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N
        jLabel6.setText("Nombres y Apellidos");

        txtnombreapellido.setFont(new java.awt.Font("Gill Sans MT", 1, 12)); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/interrogacion.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnCREARUSUARIO)
                        .addGap(18, 18, 18)
                        .addComponent(btnBACKUSUARIO))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtusuario)
                            .addComponent(txtpass, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbrol, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbagencia, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(txtidusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(txtnombreapellido, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtidusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnombreapellido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtpass, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbrol, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbagencia, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCREARUSUARIO)
                    .addComponent(btnBACKUSUARIO))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBACKUSUARIOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBACKUSUARIOActionPerformed
        this.txtidusuario.setText("");
        this.txtusuario.setText("");
        this.txtnombreapellido.setText("");
        this.txtpass.setText("");
        this.txtusuario.setEnabled(false);
        this.setVisible(false);
        Principal.dba.setVisible(true);
    }//GEN-LAST:event_btnBACKUSUARIOActionPerformed

    private void btnCREARUSUARIOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCREARUSUARIOActionPerformed
                       
        try {
            String idusuario = txtidusuario.getText();
            String nombre = txtnombreapellido.getText();
            String usuario = txtusuario.getText().toLowerCase();
            String pass = txtpass.getText();
            
            String rol = cmbrol.getSelectedItem().toString().toLowerCase();
            String nombreAgencia = cmbagencia.getSelectedItem().toString();
            
            Principal.listacampos.addLast(new Campos("id",idusuario));
            Principal.listacampos.addLast(new Campos("nombre",nombre));
            Principal.listacampos.addLast(new Campos("usuario",usuario));
            Principal.listacampos.addLast(new Campos("password",pass));
            
            LinkedList<String> validar1, validar2;
            validar1 = Principal.validar.camposVacios(Principal.listacampos);
            validar2 = Principal.validar.camposCorrectos(Principal.listacampos);
            
            if(validar1.size()>0){
                for(String error : validar1){
                    JOptionPane.showMessageDialog(null, error);
                }
                Principal.listacampos.clear();
                validar1.clear();
            }else if(validar2.size()>0){
                for(String error : validar2){
                    JOptionPane.showMessageDialog(null, error);
                }
                Principal.listacampos.clear();
                validar2.clear();
            }else{
                //todo correcto                
                if (!(rol.equals("-"))) {
                    bd.conectar();
                    int idagencia2 = bd.retornarID(nombreAgencia,"agencia");                 
                    boolean idusuario2 = bd.consultarID(idusuario,"usuario");
                    if (idagencia2!=-1) {
                        if (!idusuario2) {
                            int id = Integer.valueOf(idusuario);
                            String consulta = "INSERT INTO USUARIO VALUES(" + id + "," + "'" + nombre + "'" + ","
                                    + "'" + usuario + "', '" + pass + "' ," + "'" + rol + "' ," + idagencia2 + ")";                            
                            ConexionBD.estado.execute(consulta);
                            JOptionPane.showMessageDialog(null, "Usuario registrado exitosamente");
                            ConexionBD.conexion.close();
                            this.txtidusuario.setText("");
                            this.txtusuario.setText("");
                            this.txtnombreapellido.setText("");
                            this.txtpass.setText("");
                            this.txtusuario.setEnabled(false);
                        }else{
                            JOptionPane.showMessageDialog(null,"El id del usuario ya fue ingresado a la base de datos"
                                                              +"\nNo pueden existir datos duplicados.");
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"Error al retornar el id de la agencia");
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Error... No se ha definido un rol");
                }
            }                                    
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No es posible realizar la consulta");
        }        
    }//GEN-LAST:event_btnCREARUSUARIOActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JOptionPane.showMessageDialog(null,"Se debe colocar la inicial del primer nombre y el apellido completo\n"+"Ejemplo: JPerez");
        txtusuario.setEnabled(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(crearUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(crearUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(crearUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(crearUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new crearUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBACKUSUARIO;
    private javax.swing.JButton btnCREARUSUARIO;
    private javax.swing.JComboBox cmbagencia;
    private javax.swing.JComboBox cmbrol;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtidusuario;
    private javax.swing.JTextField txtnombreapellido;
    private javax.swing.JPasswordField txtpass;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
