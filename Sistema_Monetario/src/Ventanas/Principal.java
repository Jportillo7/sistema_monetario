package Ventanas;

import java.util.LinkedList;
import sistema_monetario.Campos;
import sistema_monetario.Validaciones;

/**
 *
 * @author jose
 */
public class Principal {
    
    public static Login login = new Login();
    public static DBA dba = new DBA();
    public static Empleado empleado = new Empleado();    
    public static Receptor_Pagador receptorpagador = new Receptor_Pagador();
    public static ventana_depositos deposito = new ventana_depositos();
    public static Deposito_efectivo depositoefectivo = new Deposito_efectivo();    
    public static Deposito_cheques depositocheques = new Deposito_cheques();
    public static Deposito_Otros_Bancos depositobancoexterno = new Deposito_Otros_Bancos();
    public static Consultar_Saldo consultadesaldo = new Consultar_Saldo();
    public static Transferencia_Fondos transferencias = new Transferencia_Fondos();
    
    public static ABC_Cuenta abc_cuenta = new ABC_Cuenta();
    public static crearCuenta cuenta = new crearCuenta();
    public static actualizarCuenta actualizarcuenta = new actualizarCuenta();
    public static Bloquear_cancelar_cuenta bloquearcancelar_cuenta = new Bloquear_cancelar_cuenta();
    
    public static crearUsuario usuario = new crearUsuario();
    
    public static Validaciones validar = new Validaciones();
    public static LinkedList<Campos> listacampos = new LinkedList<>();
    
    public static ABC_Cliente abc_cliente = new ABC_Cliente();
    public static crearCliente crearcliente = new crearCliente();
    public static actualizarCliente actualizarcliente = new actualizarCliente();    
    public static eliminarCliente eliminarcliente = new eliminarCliente();
    
    public static crearChequera crearchequera = new crearChequera();
    public static cheque_RobadoExtraviado reportarcheque = new cheque_RobadoExtraviado();            
    
}
