/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

/**
 *
 * @author jose
 */
public class Receptor_Pagador extends javax.swing.JFrame {
    
    public Receptor_Pagador() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnPAGOCHEQUES1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnCONSULTADESALDO = new javax.swing.JButton();
        btnBACK = new javax.swing.JButton();
        btnDEPSITOMONETARIO = new javax.swing.JButton();
        btnPAGOCHEQUES2 = new javax.swing.JButton();
        btnPAGOCHEQUES3 = new javax.swing.JButton();
        txtusuario = new javax.swing.JLabel();
        txtlogueado = new javax.swing.JLabel();

        btnPAGOCHEQUES1.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnPAGOCHEQUES1.setText("PAGO DE CHEQUES");
        btnPAGOCHEQUES1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPAGOCHEQUES1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ventana Receptor Pagador");

        jPanel1.setBackground(new java.awt.Color(153, 204, 255));

        btnCONSULTADESALDO.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnCONSULTADESALDO.setText("CONSULTA DE SALDO");
        btnCONSULTADESALDO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCONSULTADESALDOActionPerformed(evt);
            }
        });

        btnBACK.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnBACK.setText("LOGOUT");
        btnBACK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBACKActionPerformed(evt);
            }
        });

        btnDEPSITOMONETARIO.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnDEPSITOMONETARIO.setText("DEPOSITO MONETARIO");
        btnDEPSITOMONETARIO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDEPSITOMONETARIOActionPerformed(evt);
            }
        });

        btnPAGOCHEQUES2.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnPAGOCHEQUES2.setText("PAGO DE CHEQUES");
        btnPAGOCHEQUES2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPAGOCHEQUES2ActionPerformed(evt);
            }
        });

        btnPAGOCHEQUES3.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnPAGOCHEQUES3.setText("TRANSFERENCIA DE FONDOS");
        btnPAGOCHEQUES3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPAGOCHEQUES3ActionPerformed(evt);
            }
        });

        txtlogueado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 168, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtlogueado, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBACK, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPAGOCHEQUES3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPAGOCHEQUES2, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCONSULTADESALDO, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDEPSITOMONETARIO, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(txtlogueado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(btnDEPSITOMONETARIO, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPAGOCHEQUES2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCONSULTADESALDO, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPAGOCHEQUES3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(btnBACK, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCONSULTADESALDOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCONSULTADESALDOActionPerformed
        this.dispose();
        Principal.consultadesaldo.setVisible(true);
    }//GEN-LAST:event_btnCONSULTADESALDOActionPerformed

    private void btnBACKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBACKActionPerformed
        this.dispose();
        Principal.login.setVisible(true);
    }//GEN-LAST:event_btnBACKActionPerformed

    private void btnDEPSITOMONETARIOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDEPSITOMONETARIOActionPerformed
        this.dispose();
        Principal.deposito.setVisible(true);
    }//GEN-LAST:event_btnDEPSITOMONETARIOActionPerformed

    private void btnPAGOCHEQUES1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPAGOCHEQUES1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPAGOCHEQUES1ActionPerformed

    private void btnPAGOCHEQUES2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPAGOCHEQUES2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPAGOCHEQUES2ActionPerformed

    private void btnPAGOCHEQUES3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPAGOCHEQUES3ActionPerformed
        this.dispose();
        Principal.transferencias.setVisible(true);
    }//GEN-LAST:event_btnPAGOCHEQUES3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Receptor_Pagador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Receptor_Pagador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Receptor_Pagador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Receptor_Pagador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Receptor_Pagador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBACK;
    private javax.swing.JButton btnCONSULTADESALDO;
    private javax.swing.JButton btnDEPSITOMONETARIO;
    private javax.swing.JButton btnPAGOCHEQUES1;
    private javax.swing.JButton btnPAGOCHEQUES2;
    private javax.swing.JButton btnPAGOCHEQUES3;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JLabel txtlogueado;
    private javax.swing.JLabel txtusuario;
    // End of variables declaration//GEN-END:variables
}
