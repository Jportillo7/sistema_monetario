/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.sql.CallableStatement;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import sistema_monetario.Campos;
import sistema_monetario.ConexionBD;
import static sistema_monetario.ConexionBD.conexion;
import static sistema_monetario.ConexionBD.estado;
import static sistema_monetario.ConexionBD.resultado;

/**
 *
 * @author jose
 */
public class actualizarCliente extends javax.swing.JFrame {

    ConexionBD bd = new ConexionBD();
    
    public actualizarCliente() {
        initComponents();        
        this.txtnombre.setEnabled(false);
        this.txttelefono.setEnabled(false);
        this.txtdireccion.setEnabled(false);
        this.txtcorreo.setEnabled(false);
        this.btnBUSCAR.setEnabled(true);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtdpi = new javax.swing.JTextField();
        btnBACKCLIENTE = new javax.swing.JButton();
        txttelefono = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        btnATUALIZAR_CLIENTE = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txtdia = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtcorreo = new javax.swing.JTextField();
        txtmes = new javax.swing.JTextField();
        txtanio = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lbltipo = new javax.swing.JLabel();
        btnBUSCAR = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 204, 255));

        jLabel1.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Nombre:");

        jLabel3.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Telefono:");

        jLabel4.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Direccion:");

        txtdireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdireccionActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("DPI:");

        txtdpi.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtdpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdpiActionPerformed(evt);
            }
        });
        txtdpi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdpiKeyTyped(evt);
            }
        });

        btnBACKCLIENTE.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnBACKCLIENTE.setText("BACK");
        btnBACKCLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBACKCLIENTEActionPerformed(evt);
            }
        });

        txttelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttelefonoActionPerformed(evt);
            }
        });
        txttelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txttelefonoKeyTyped(evt);
            }
        });

        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });

        btnATUALIZAR_CLIENTE.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnATUALIZAR_CLIENTE.setText("ACTUALIZAR");
        btnATUALIZAR_CLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnATUALIZAR_CLIENTEActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Fecha Nacimiento:");

        txtdia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdiaActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Correo:");

        txtcorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcorreoActionPerformed(evt);
            }
        });

        jLabel5.setText("dia");

        jLabel6.setText("mes");

        jLabel9.setText("año");

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("ejemplo_correo1@gmail.com");

        lbltipo.setFont(new java.awt.Font("Gill Sans MT", 1, 18)); // NOI18N
        lbltipo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbltipo.setText("ACTUALIZACION DE DATOS DEL CLIENTE");

        btnBUSCAR.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnBUSCAR.setText("Buscar");
        btnBUSCAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBUSCARActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnATUALIZAR_CLIENTE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBACKCLIENTE, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnBUSCAR)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txttelefono, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtdia, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabel5)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtmes, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabel6)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabel9))
                                            .addComponent(txtanio, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtcorreo))))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lbltipo, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbltipo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBUSCAR)
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtmes, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtdia, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtanio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnATUALIZAR_CLIENTE, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBACKCLIENTE, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtdireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionActionPerformed

    private void txtdpiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdpiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdpiActionPerformed

    private void btnBACKCLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBACKCLIENTEActionPerformed
        this.txtdpi.setText("");
        this.txtnombre.setText("");
        this.txtcorreo.setText("");
        this.txttelefono.setText("");
        this.txtdireccion.setText("");
        this.txtdpi.setEnabled(true);
        this.txtnombre.setEnabled(false);
        this.txttelefono.setEnabled(false);
        this.txtdireccion.setEnabled(false);
        this.txtcorreo.setEnabled(false);    
        this.btnBUSCAR.setEnabled(true);
        this.dispose();
        Principal.abc_cliente.setVisible(true);
    }//GEN-LAST:event_btnBACKCLIENTEActionPerformed

    private void txttelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttelefonoActionPerformed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void btnATUALIZAR_CLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnATUALIZAR_CLIENTEActionPerformed
        
        try {
            String dpi = txtdpi.getText();
            String nombre = txtnombre.getText();
            /*int dia = Integer.parseInt(txtdia.getText());
             int mes = Integer.parseInt(txtmes.getText());
             int anio = Integer.parseInt(txtanio.getText());*/
            String correo = txtcorreo.getText();
            String telefono = txttelefono.getText();
            String direccion = txtdireccion.getText();

            /*Agregando los campos a una lista que contiene al objeto Campos*/
            Principal.listacampos.addLast(new Campos("dpi", dpi));
            Principal.listacampos.addLast(new Campos("nombre", nombre));
            Principal.listacampos.addLast(new Campos("correo", correo));
            Principal.listacampos.addLast(new Campos("telefono", telefono));
            Principal.listacampos.addLast(new Campos("direccion", direccion));

            LinkedList<String> mensajes, mensajes2;
            mensajes = Principal.validar.camposVacios(Principal.listacampos);
            mensajes2 = Principal.validar.camposCorrectos(Principal.listacampos);

            if (mensajes.size() > 0) {                
                for (String mensaje : mensajes) {
                    JOptionPane.showMessageDialog(null, mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                mensajes.clear();
            } else if (mensajes2.size() > 0) {
                for (String mensaje : mensajes2) {
                    JOptionPane.showMessageDialog(null, mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                mensajes2.clear();
            }else if(telefono.length() <8){
                JOptionPane.showMessageDialog(null, "El campo telefono debe contener 8 digitos","Advertencia", JOptionPane.WARNING_MESSAGE);
            } else {
                /*Conectando a la base de datos*/
                Long dpi2 = Long.parseLong(dpi);
                int telefono2 = Integer.valueOf(telefono);
                bd.conectar();                
                ConexionBD.call = conexion.prepareCall("{call actualizar_tabla_cliente(?,?,?,?,?,?)}");
                ConexionBD.call.setLong(1, dpi2);
                ConexionBD.call.setString(2,nombre);
                ConexionBD.call.setString(3,correo);
                ConexionBD.call.setInt(4,telefono2);
                ConexionBD.call.setString(5,direccion);
                ConexionBD.call.setString(6,"activo");
                ConexionBD.call.execute();
                
//                String consulta = "UPDATE cliente SET nombre = '" + nombre + "',"
//                        + "correo = '" + correo + "',"
//                        + "telefono = " + telefono + ","
//                        + "direccion = '" + direccion + "'"
//                        + " WHERE dpi = " + dpi2;
//                ConexionBD.estado.executeQuery(consulta);
                JOptionPane.showMessageDialog(null, "Cliente actualizado exitosamente","Exito",JOptionPane.INFORMATION_MESSAGE);
                //resultado.close();
                conexion.close();
                this.txtdpi.setText("");
                this.txtnombre.setText("");
                this.txtcorreo.setText("");
                this.txttelefono.setText("");
                this.txtdireccion.setText("");
                this.txtdpi.setEnabled(true);
                this.txtnombre.setEnabled(false);
                this.txttelefono.setEnabled(false);
                this.txtdireccion.setEnabled(false);
                this.txtcorreo.setEnabled(false);
                this.btnBUSCAR.setEnabled(true);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No es posible realizar la actualizacion","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnATUALIZAR_CLIENTEActionPerformed

    private void txtdiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdiaActionPerformed

    private void txtcorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcorreoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoActionPerformed

    private void btnBUSCARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBUSCARActionPerformed
        String dpi = txtdpi.getText();
        Principal.listacampos.addLast(new Campos("dpi",dpi));
                
        LinkedList<String> validar1, validar2;
        validar1 = Principal.validar.camposVacios(Principal.listacampos);
        validar2 = Principal.validar.camposCorrectos(Principal.listacampos);
        if(validar1.size()>0){
            for(String mensaje : validar1){
                JOptionPane.showMessageDialog(null,mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);                
            }
            Principal.listacampos.clear();
            validar1.clear();
        }else if(validar2.size()>0){
            for(String mensaje : validar2){
                JOptionPane.showMessageDialog(null,mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);                
            }
            Principal.listacampos.clear();
            validar2.clear();
        } else {
            //No existe ningun tipo de error...
            try {
                Long id = Long.parseLong(dpi);
                bd.conectar();
                String consulta = "SELECT nombre,correo,telefono,direccion FROM CLIENTE where"
                        + " dpi = " + id;
                resultado = ConexionBD.estado.executeQuery(consulta);
                if (ConexionBD.resultado.next()) {
                    this.txtnombre.setEnabled(true);
                    this.txttelefono.setEnabled(true);
                    this.txtdireccion.setEnabled(true);
                    this.txtcorreo.setEnabled(true);
                    this.txtdpi.setEnabled(false);
                    this.txtnombre.setText(resultado.getString("nombre"));
                    this.txtcorreo.setText(resultado.getString("correo"));
                    this.txttelefono.setText(resultado.getString("telefono"));
                    this.txtdireccion.setText(resultado.getString("direccion"));
                    this.btnBUSCAR.setEnabled(false);
                    resultado.close();
                    ConexionBD.conexion.close();                    
                } else {
                    JOptionPane.showMessageDialog(null, "El cliente no existe en la base de datos\n No es posible mostrar su informacion para actualizarla","Advertencia",JOptionPane.WARNING_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta.","Error-Actualizar_Cliente",JOptionPane.ERROR_MESSAGE);
            }            
        }
    }//GEN-LAST:event_btnBUSCARActionPerformed

    private void txtdpiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdpiKeyTyped
        int caracteres = 13;
        if (this.txtdpi.getText().length() >= caracteres) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "EL numero de caracteres en el campo dpi"
                    + "\ndebe ser igual a 13", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtdpiKeyTyped

    private void txttelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonoKeyTyped
        int caracteres = 8;
        if (this.txtdpi.getText().length() >= caracteres) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "El numero excede de limite permitido"
                    + "\ndebe contener 8 digitos", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txttelefonoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new actualizarCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnATUALIZAR_CLIENTE;
    private javax.swing.JButton btnBACKCLIENTE;
    private javax.swing.JButton btnBUSCAR;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbltipo;
    private javax.swing.JTextField txtanio;
    private javax.swing.JTextField txtcorreo;
    private javax.swing.JTextField txtdia;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtdpi;
    private javax.swing.JTextField txtmes;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
