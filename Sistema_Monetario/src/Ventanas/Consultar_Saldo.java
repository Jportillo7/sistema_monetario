/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.util.LinkedList;
import javax.swing.JOptionPane;
import sistema_monetario.Campos;
import sistema_monetario.ConexionBD;

/**
 *
 * @author jose
 */
public class Consultar_Saldo extends javax.swing.JFrame {

    ConexionBD bd = new ConexionBD();
    public Consultar_Saldo() {
        initComponents();
        this.txtidcuenta.removeAllItems();
        this.txtidcuenta.setEnabled(false);
        this.btnCONSULTARSALDO.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtdpi = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnCONSULTARSALDO = new javax.swing.JButton();
        btnBACKCUENTA = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtmonto = new javax.swing.JTextField();
        btnVERIFICAR = new javax.swing.JButton();
        txtidcuenta = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 204, 255));

        jLabel11.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("DPI:");

        txtdpi.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtdpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdpiActionPerformed(evt);
            }
        });
        txtdpi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdpiKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Id cuenta:");

        btnCONSULTARSALDO.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnCONSULTARSALDO.setText("CONSULTAR");
        btnCONSULTARSALDO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCONSULTARSALDOActionPerformed(evt);
            }
        });

        btnBACKCUENTA.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnBACKCUENTA.setText("BACK");
        btnBACKCUENTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBACKCUENTAActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Gill Sans MT", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("CONSULTA DE SALDO");

        jLabel9.setFont(new java.awt.Font("Gill Sans MT", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("SALDO ACTUAL:");

        txtmonto.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        txtmonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtmonto.setEnabled(false);
        txtmonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmontoActionPerformed(evt);
            }
        });

        btnVERIFICAR.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        btnVERIFICAR.setText("Verificar");
        btnVERIFICAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVERIFICARActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVERIFICAR, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCONSULTARSALDO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBACKCUENTA, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtmonto, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtidcuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtdpi, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVERIFICAR)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtidcuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmonto, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBACKCUENTA, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(btnCONSULTARSALDO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtdpiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdpiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdpiActionPerformed

    private void btnCONSULTARSALDOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCONSULTARSALDOActionPerformed
        try {
            String campoidcuenta = txtidcuenta.getSelectedItem().toString();
            String campodpi = txtdpi.getText();

            bd.conectar();
            boolean bandera_verificada = false;
            String consultadatos = "SELECT * FROM detalle_cliente "
                    + "WHERE idcliente = " + Long.parseLong(campodpi)
                    + " AND idcuenta = " + Integer.valueOf(campoidcuenta);
            ConexionBD.resultado = ConexionBD.estado.executeQuery(consultadatos);
            while (ConexionBD.resultado.next()) {
                bandera_verificada = true;
            }
            ConexionBD.conexion.close();

            /*Si los datos de la cuenta corresponden al cliente se muestra el monto*/
            if (bandera_verificada) {
                bd.conectar();
                String estado_cuenta = "";
                String consulta_estado_cuenta = "SELECT estado FROM cuenta WHERE idcuenta = "+ Integer.valueOf(campoidcuenta);
                ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_estado_cuenta);
                while(ConexionBD.resultado.next()){
                    estado_cuenta = ConexionBD.resultado.getString("estado");
                }
                ConexionBD.conexion.close();
                
                if (estado_cuenta.equalsIgnoreCase("activa") || estado_cuenta.equalsIgnoreCase("activada")) {
                    bd.conectar();
                    String consulta_monto = "SELECT monto FROM cuenta WHERE idcuenta = "
                            + Integer.valueOf(campoidcuenta);
                    ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_monto);
                    while (ConexionBD.resultado.next()) {
                        this.txtmonto.setText("Q." + ConexionBD.resultado.getDouble("monto"));
                    }
                    ConexionBD.conexion.close();
                    this.txtdpi.setEnabled(false);
                    this.btnVERIFICAR.setEnabled(false);
                }else{
                    this.txtidcuenta.removeAllItems();
                    this.txtidcuenta.setEnabled(false);
                    this.txtmonto.setText("");
                    this.txtdpi.setEnabled(true);
                    this.btnVERIFICAR.setEnabled(true);
                    JOptionPane.showMessageDialog(null,"No se puede consultar el saldo de una cuenta"
                            + " con estado "+estado_cuenta,"Advertencia",JOptionPane.WARNING_MESSAGE);
                    
                }                
                
            } else {
                bd.conectar();
                String nombrecliente = "";
                String consulta_nombreCliente = "SELECT nombre FROM cliente WHERE dpi = "+Long.parseLong(campodpi);
                ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_nombreCliente);
                while(ConexionBD.resultado.next()){
                    nombrecliente = ConexionBD.resultado.getString("nombre");
                }
                JOptionPane.showMessageDialog(null, "No es posible consultar el saldo debido a que la cuenta: " + campoidcuenta
                        + " no le pertenece al cliente "+nombrecliente);
            }            
        } catch (Exception e) {
            System.out.println("error capturado "+e);
            JOptionPane.showMessageDialog(null, "No es posible consultar el saldo de la cuenta","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCONSULTARSALDOActionPerformed

    private void btnBACKCUENTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBACKCUENTAActionPerformed
        this.txtdpi.setText("");        
        this.txtmonto.setText("");
        this.txtidcuenta.removeAllItems();
        this.txtidcuenta.setEnabled(false);
        this.btnCONSULTARSALDO.setEnabled(false);
        this.txtdpi.setEnabled(true);
        this.btnVERIFICAR.setEnabled(true);
        this.dispose();
        Principal.receptorpagador.setVisible(true);
    }//GEN-LAST:event_btnBACKCUENTAActionPerformed

    private void txtmontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmontoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmontoActionPerformed

    private void btnVERIFICARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVERIFICARActionPerformed
        try {
            String campoidcliente = this.txtdpi.getText();            
            Principal.listacampos.addLast(new Campos("dpi",campoidcliente));            
            LinkedList<String> mensaje1, mensaje2;
            mensaje1 = Principal.validar.camposVacios(Principal.listacampos);
            mensaje2 = Principal.validar.camposCorrectos(Principal.listacampos);
            
            if(mensaje1.size()>0){
                for(String error : mensaje1){
                    JOptionPane.showMessageDialog(null,error,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                mensaje1.clear();
            }else if(mensaje2.size()>0){
                for(String error : mensaje2){
                    JOptionPane.showMessageDialog(null,error,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                Principal.listacampos.clear();
                mensaje2.clear();
            } else{                
                bd.conectar();
                boolean existe_dpi_cliente = bd.consultarDPI(Long.parseLong(campoidcliente));
                ConexionBD.conexion.close();
                if(existe_dpi_cliente){
                    bd.conectar();
                    String consulta_cuentas_cliente = "SELECT idcuenta FROM detalle_cliente"
                            + " WHERE idcliente = "+Long.parseLong(campoidcliente);
                    ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_cuentas_cliente);
                    while(ConexionBD.resultado.next()){
                        this.txtidcuenta.addItem(ConexionBD.resultado.getInt("idcuenta"));
                    }
                    ConexionBD.conexion.close();
                    
                    if(this.txtidcuenta.getItemCount()>0){                        
                        this.txtidcuenta.setEnabled(true);
                        this.btnCONSULTARSALDO.setEnabled(true);
                        this.txtdpi.setEnabled(false);
                        this.btnVERIFICAR.setEnabled(false);
                    }else{
                        bd.conectar();
                        String cliente = "";
                        String consulta_cliente = "SELECT nombre FROM cliente WHERE dpi = "+Long.parseLong(campoidcliente);
                        ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_cliente);
                        while(ConexionBD.resultado.next()){
                            cliente = ConexionBD.resultado.getString("nombre");
                        }
                        JOptionPane.showMessageDialog(null,"No existe ninguna cuenta asociada al cliente "+cliente
                            ,"Advertencia",JOptionPane.WARNING_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"EL dpi del cliente no existe en la base de datos"
                            ,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
            }                        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"No es posible verificar el dpi del cliente","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnVERIFICARActionPerformed

    private void txtdpiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdpiKeyTyped
        int caracteres = 13;
        if (this.txtdpi.getText().length() >= caracteres) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "EL numero de caracteres en el campo dpi"
                    + "\ndebe ser igual a 13", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtdpiKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Consultar_Saldo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Consultar_Saldo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Consultar_Saldo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Consultar_Saldo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Consultar_Saldo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBACKCUENTA;
    private javax.swing.JButton btnCONSULTARSALDO;
    private javax.swing.JButton btnVERIFICAR;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtdpi;
    private javax.swing.JComboBox txtidcuenta;
    private javax.swing.JTextField txtmonto;
    // End of variables declaration//GEN-END:variables
}
