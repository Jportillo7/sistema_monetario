/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

/**
 *
 * @author jose
 */
public class ABC_Cliente extends javax.swing.JFrame {

    /**
     * Creates new form ABC_Cliente
     */
    public ABC_Cliente() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCREAR_CLIENTE = new javax.swing.JButton();
        btnACTUALIZAR_CLIENTE = new javax.swing.JButton();
        btnELIMINAR_CLIENTE = new javax.swing.JButton();
        btn_BACK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ABC Clientes");
        setBackground(new java.awt.Color(153, 204, 255));

        btnCREAR_CLIENTE.setFont(new java.awt.Font("Gill Sans MT", 0, 18)); // NOI18N
        btnCREAR_CLIENTE.setText("Crear Cliente");
        btnCREAR_CLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCREAR_CLIENTEActionPerformed(evt);
            }
        });

        btnACTUALIZAR_CLIENTE.setFont(new java.awt.Font("Gill Sans MT", 0, 18)); // NOI18N
        btnACTUALIZAR_CLIENTE.setText("Actualizar Cliente");
        btnACTUALIZAR_CLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnACTUALIZAR_CLIENTEActionPerformed(evt);
            }
        });

        btnELIMINAR_CLIENTE.setFont(new java.awt.Font("Gill Sans MT", 0, 18)); // NOI18N
        btnELIMINAR_CLIENTE.setText("Elliminar Cliente");
        btnELIMINAR_CLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnELIMINAR_CLIENTEActionPerformed(evt);
            }
        });

        btn_BACK.setFont(new java.awt.Font("Gill Sans MT", 0, 18)); // NOI18N
        btn_BACK.setText("Back");
        btn_BACK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BACKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(77, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btn_BACK)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnACTUALIZAR_CLIENTE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCREAR_CLIENTE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnELIMINAR_CLIENTE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(77, 77, 77))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(btnCREAR_CLIENTE)
                .addGap(18, 18, 18)
                .addComponent(btnACTUALIZAR_CLIENTE)
                .addGap(18, 18, 18)
                .addComponent(btnELIMINAR_CLIENTE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addComponent(btn_BACK)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCREAR_CLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCREAR_CLIENTEActionPerformed
        Principal.crearcliente.setVisible(true);
    }//GEN-LAST:event_btnCREAR_CLIENTEActionPerformed

    private void btnACTUALIZAR_CLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnACTUALIZAR_CLIENTEActionPerformed
        Principal.actualizarcliente.setVisible(true);
    }//GEN-LAST:event_btnACTUALIZAR_CLIENTEActionPerformed

    private void btn_BACKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BACKActionPerformed
        this.setVisible(false);
        Principal.empleado.setVisible(true);
    }//GEN-LAST:event_btn_BACKActionPerformed

    private void btnELIMINAR_CLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnELIMINAR_CLIENTEActionPerformed
        this.setVisible(false);        
        Principal.eliminarcliente.setVisible(true);
    }//GEN-LAST:event_btnELIMINAR_CLIENTEActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ABC_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ABC_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ABC_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ABC_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABC_Cliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnACTUALIZAR_CLIENTE;
    private javax.swing.JButton btnCREAR_CLIENTE;
    private javax.swing.JButton btnELIMINAR_CLIENTE;
    private javax.swing.JButton btn_BACK;
    // End of variables declaration//GEN-END:variables
}
