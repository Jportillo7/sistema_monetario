package Ventanas;

import static Ventanas.Principal.listacampos;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import sistema_monetario.Campos;
import sistema_monetario.ConexionBD;

/**
 *
 * @author jose
 */
public class Login extends javax.swing.JFrame {

    ConexionBD bd = new ConexionBD();            
    
    public Login() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnlogin = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbrol = new javax.swing.JComboBox();
        txtpassword = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema Monetario");

        btnlogin.setText("LOGIN");
        btnlogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnloginActionPerformed(evt);
            }
        });

        jLabel1.setText("Usuario");

        jLabel2.setText("Password");

        jLabel3.setText("ROL");

        cmbrol.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "DBA", "CAJERO", "AUDITOR", "OPERADOR", "GRABADOR", "RECEPTOR_PAGADOR", "ATENCION_AL_CLIENTE" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtusuario)
                    .addComponent(txtpassword)
                    .addComponent(cmbrol, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(60, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnlogin, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(111, 111, 111))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtpassword, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(cmbrol, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE))
                .addGap(29, 29, 29)
                .addComponent(btnlogin, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnloginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnloginActionPerformed
        
        try {
            /*Obteniendo los datos de las cajas de texto*/
            String usuario = txtusuario.getText();
            String pass = txtpassword.getText();
            String rol = cmbrol.getSelectedItem().toString().toLowerCase();            
                                                
            /*Verificando validaciones*/     
            Principal.listacampos.addLast(new Campos("usuario",usuario));
            Principal.listacampos.addLast(new Campos("password",pass));            
            
            LinkedList<String> mensajes, mensajes2;
            mensajes = Principal.validar.camposVacios(listacampos);
            mensajes2 = Principal.validar.camposCorrectos(listacampos);
            
            if(mensajes.size()>0){
                for (String mensaje : mensajes) {
                    JOptionPane.showMessageDialog(null, mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                listacampos.clear();
                mensajes.clear();
            }else if(mensajes2.size()>0){
                for (String mensaje : mensajes2) {
                    JOptionPane.showMessageDialog(null, mensaje,"Advertencia",JOptionPane.WARNING_MESSAGE);
                }
                listacampos.clear();
                mensajes2.clear();
            }else{
                if (!(rol.equals("-"))) {
                    /*Realizando la conexion a la base de datos*/
                    bd.conectar();                    
                    String login = bd.consultarLogin(usuario, pass, rol);
                    ConexionBD.conexion.close();
                    
                    bd.conectar();
                    boolean existe_usuario = false;
                    String consulta_existe_usuario = "SELECT idusuario FROM usuario WHERE usuario = '"+usuario+"'";
                    ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_existe_usuario);
                    while(ConexionBD.resultado.next()){
                        existe_usuario = true;
                    }
                    ConexionBD.conexion.close();
                    
                    bd.conectar();
                    String estado_usuario = "";
                    String consulta_estado_usuario = "SELECT estado FROM usuario WHERE usuario = '"+usuario+"'";
                    ConexionBD.resultado = ConexionBD.estado.executeQuery(consulta_estado_usuario);
                    while(ConexionBD.resultado.next()){
                        estado_usuario = ConexionBD.resultado.getString("estado");
                    }
                    ConexionBD.conexion.close();
                    
                    
                    if (existe_usuario) {
                        if (estado_usuario.equalsIgnoreCase("activo")) {
                            switch (login) {
                                case "dba":
                                    JOptionPane.showMessageDialog(null, "Autenticacion correcta!", "Administrador", JOptionPane.INFORMATION_MESSAGE);
                                    Principal.dba.setVisible(true);
                                    this.setVisible(false);
                                    break;
                                case "atencion_al_cliente":
                                    //JOptionPane.showMessageDialog(null, "Autenticacion correcta!", "Atencion al cliente", JOptionPane.INFORMATION_MESSAGE);
                                    Principal.empleado.setVisible(true);
                                    Principal.empleado.txtlogueado.setText(usuario);
                                    this.setVisible(false);
                                    break;
                                case "receptor_pagador":
                                    //JOptionPane.showMessageDialog(null, "Autenticacion correcta!", "Receptor_Pagador", JOptionPane.INFORMATION_MESSAGE);
                                    Principal.receptorpagador.setVisible(true);
                                    Receptor_Pagador.txtlogueado.setText(usuario);
                                    this.setVisible(false);
                                    break;
                                case "auditor":
                                    JOptionPane.showMessageDialog(null, "Autenticacion correcta!", "Auditor", JOptionPane.INFORMATION_MESSAGE);
                                    Principal.empleado.setVisible(true);
                                    this.setVisible(false);
                                    break;
                                case "gerente":
                                    JOptionPane.showMessageDialog(null, "Autenticacion correcta!", "Gerente", JOptionPane.INFORMATION_MESSAGE);
                                    Principal.empleado.setVisible(true);
                                    this.setVisible(false);
                                    break;
                                default:
                                    JOptionPane.showMessageDialog(null, "Error de autenticacion", "Login", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "El usuario no se encuentra activo", "Advertencia", JOptionPane.WARNING_MESSAGE);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"El usuario no existe en la base de datos","Advertencia",JOptionPane.WARNING_MESSAGE);
                    }
                    listacampos.clear();                    
                } else {
                    JOptionPane.showMessageDialog(null, "No se establecio un ROL","Rol",JOptionPane.WARNING_MESSAGE);
                }
                
            }                                                                          
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"No se ha podido establecer una conexion con la base de datos.","Login-consulta",JOptionPane.ERROR_MESSAGE);
        }     
    }//GEN-LAST:event_btnloginActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnlogin;
    private javax.swing.JComboBox cmbrol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField txtpassword;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
